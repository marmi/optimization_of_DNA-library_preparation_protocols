#!/bin/bash
source /storage/data1/marmi/miniconda/installation/etc/profile.d/conda.sh
conda activate marmi_methylome

#перед каждым запуском заполнять эти пять переменных и path_to_sample
input_file="libs_11_12_23.csv"
cell="V350165701"
path_to_run="/storage/data1/aug/V350165701_repair"
goal_dir="custom_libs_E_Coli_Zvl"
path_to_assembly="/storage/data1/marmi/dec_custom_libs"

path_to_python="/storage/data1/marmi/trial_scripts/split_assembly.py"
path_to_python_chr="/storage/data1/marmi/trial_scripts/chromosome.py"
#выкидываем заголовок и заменяем пробелы на нижние подчеркивания
cp $input_file source_csv.csv
sed -i 's/ /_/g' source_csv.csv

<source_csv.csv awk -v a_cell="${cell}" -F"," 'NR!=1{print $1 " " a_cell "_L0" $4 "_" $3 "_1.fq.gz" " " a_cell "_L0" $4 "_" $3 "_2.fq.gz" " " $2 " " $4}' >reads_fastp.txt
rm source_csv.csv

cat reads_fastp.txt | while read i || [[ -n $i ]];
do
dir=`echo $i| awk '{split($0, array); print array[1]}'`
if [ "$dir" == "$goal_dir" ]; then
    fn1=`echo $i| awk '{split($0, array); print array[2]}'`
    fn2=`echo $i| awk '{split($0, array); print array[3]}'`
    sample=`echo $i| awk '{split($0, array); print array[4]}'`
    lane=`echo $i| awk '{split($0, array); print array[5]}'`
    path_to_sample="/storage/data1/marmi/dec_custom_libs_repaired_${sample}"
    mkdir ${path_to_sample}
    mkdir ${path_to_sample}/qc_source_files
    cd ${path_to_sample}/qc_source_files
    read1="${path_to_run}/L0${lane}/${fn1}"
    read2="${path_to_run}/L0${lane}/${fn2}"
    fastp -e 30 -w 7 --in1 ${read1} --in2 ${read2} \
        --out1 custom_${sample}_1.fastq.gz --out2 custom_${sample}_2.fastq.gz \
        --unpaired1 custom_${sample}_u.fastq.gz --unpaired2 custom_${sample}_u.fastq.gz \
        1>fastp_custom_${sample}_output.txt 2>fastp_custom_${sample}_err.txt
fi
done
#НЕ УДАЛЯТЬ READS_FASTP.TXT

conda deactivate
conda activate marmi_telegram
telegram-send "Work_custom_union_assembly first step ends at `date`."
conda deactivate