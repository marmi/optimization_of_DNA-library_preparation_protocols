#!/bin/bash
source /storage/data1/marmi/miniconda/installation/etc/profile.d/conda.sh
conda activate marmi_methylome
#перед каждым запуском заполнять эти три переменных и path_to_sample
path_to_assembly="/storage/data1/marmi/dec_custom_libs_zvl/unicycler_assembly_custom"
goal_dir="custom_libs_E_Coli_Zvl"
path_to_tables="/storage/data1/marmi/dec_custom_libs_zvl/info_tables_repaired"
mkdir /storage/data1/marmi/dec_custom_libs_zvl
path_to_python="/storage/data1/marmi/trial_scripts/info_pol_mapq_union.py"
mkdir ${path_to_tables}
touch ${path_to_tables}/names_of_custom_chromosome.txt
touch ${path_to_tables}/names_of_custom_plasmid.txt
cat reads_fastp.txt | while read i || [[ -n $i ]];
do
dir=`echo $i| awk '{split($0, array); print array[1]}'`
if [ "$dir" == "$goal_dir" ]; then
        sample=`echo $i| awk '{split($0, array); print array[4]}'`
        path_to_sample="/storage/data1/marmi/dec_custom_libs_repaired_${sample}"
        name_of_dir="union_${sample}"
        mkdir ${path_to_sample}/${name_of_dir}
        cd ${path_to_sample}/${name_of_dir}
        bwa mem -t 7 ${path_to_assembly}/assembly.fasta ../qc_source_files/custom_${sample}_1.fastq.gz \
                ../qc_source_files/custom_${sample}_2.fastq.gz >custom_${sample}_paired.sam \
                2>bwa_mem_custom_${sample}_paired.log
        bwa mem -t 7 ${path_to_assembly}/assembly.fasta ../qc_source_files/custom_${sample}_u.fastq.gz \
                >custom_${sample}_unpaired.sam \
                2>bwa_mem_custom_${sample}_unpaired.log
        samtools view -Sb -o custom_${sample}_paired.bam custom_${sample}_paired.sam
        samtools view -Sb -o custom_${sample}_unpaired.bam custom_${sample}_unpaired.sam
        rm custom_${sample}_paired.sam
        rm custom_${sample}_unpaired.sam
        samtools sort -o custom_${sample}_paired_sorted.bam custom_${sample}_paired.bam
        samtools sort -o custom_${sample}_unpaired_sorted.bam custom_${sample}_unpaired.bam
        samtools merge -o custom_${sample}.bam \
                custom_${sample}_paired_sorted.bam custom_${sample}_unpaired_sorted.bam
        samtools index custom_${sample}.bam
        rm custom_${sample}_paired_sorted.bam
        rm custom_${sample}_unpaired_sorted.bam
        rm custom_${sample}_paired.bam
        rm custom_${sample}_unpaired.bam
        touch custom_${sample}_genome.txt
        touch custom_${sample}_plasmid.txt
        cat ${path_to_assembly}/chromosome_genome.txt | while read j || [[ -n $j ]];
        do
                samtools view -F 4 custom_${sample}.bam $j | grep 'MD:Z' >> custom_${sample}_genome.txt
        done
        cat ${path_to_assembly}/chromosome_plasmid.txt | while read j || [[ -n $j ]];
        do
                samtools view -F 4 custom_${sample}.bam $j | grep 'MD:Z' >> custom_${sample}_plasmid.txt
        done
        python3 ${path_to_python} ${sample}_genome ${path_to_sample}/${name_of_dir}/ ${path_to_tables}/
        python3 ${path_to_python} ${sample}_plasmid ${path_to_sample}/${name_of_dir}/ ${path_to_tables}/
        echo "custom_${sample}_genome_mapq20_table.txt" >>${path_to_tables}/names_of_custom_chromosome.txt
        echo "custom_${sample}_plasmid_mapq20_table.txt" >>${path_to_tables}/names_of_custom_plasmid.txt
        
fi
done
conda deactivate
conda activate marmi_telegram
telegram-send "Work_custom_union_align second step ends at `date`."
conda deactivate
