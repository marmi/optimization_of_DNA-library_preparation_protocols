# optimization_of_DNA-library_preparation_protocols

This repository contains supplementary information for the poster session.

In the directory "Supplementary" you'll find reports with additional plots.

Scripts were used in following sequence:  
    custom_lib_fastp_zvl_without_assembly.sh  
    custom_lib_union_align_zvl.sh  
    chromosome.R/plasmid.R/...  


